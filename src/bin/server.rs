//#![feature(plugin, custom_derive)]
//#![plugin(rocket_codegen)]

//extern crate rocket;
extern crate iron;
use iron::prelude::*;
use iron::Handler;
use iron::Headers;
use iron::Url;
use iron::TypeMap;
use iron::status::Status;
use iron::response::WriteBody;

extern crate reqwest;
use reqwest::{Client, ClientBuilder};
use reqwest::header;

extern crate regex;
use regex::Regex;

extern crate serde_json;

extern crate image;
use image::ImageFormat;

extern crate base64;

//extern crate imgur;

extern crate wynnautocrop;
use wynnautocrop::*;

use std::collections::HashMap;
use std::env;
use std::io::{Cursor, Read, Write};

fn postimg_gallery(out: &mut Write, url: &str) -> Result<(), std::io::Error> {
	let html = reqwest::get(url).unwrap().text().unwrap();
	let regex = Regex::new(r"embed_value=(\{.+\});").unwrap();
	let regex2 = Regex::new(r#"href="(.*\?dl=1)""#).unwrap();
	let json = regex.captures(&html).unwrap().get(1).unwrap().as_str();
	let data: serde_json::Value = serde_json::from_str(json).unwrap();
	//let mut acc = String::new();
	let mut headers = header::Headers::new();
	headers.set(header::UserAgent::new("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36".to_string()));
	let client = ClientBuilder::new()
		.default_headers(headers)
		.build().unwrap();
	//let handle = imgur::Handle::new("c819d6faf0219ac".to_owned());
	for key in data.as_object().unwrap().keys() {
		let url = format!("https://postimg.org/image/{}/", key);
		println!("getting {:?}", url);
		let html = reqwest::get(&url).unwrap().text().unwrap();
		let url = regex2.captures(&html).unwrap().get(1).unwrap().as_str();
		println!("getting {:?}", url);

		let mut buf = Vec::new();
		loop {
			if let Ok(mut res) = client.get(url).send() {
				if let Ok(_) = res.read_to_end(&mut buf) {
					if let Ok(format) = image::guess_format(&buf) {
						if let Ok(mut img) = image::load(Cursor::new(&*buf), format) {
							img = crop(img);
							let mut buf = Vec::new();
							img.save(&mut buf, ImageFormat::PNG).unwrap();
							//let upload = handle.upload(&buf).unwrap();						
							//println!("uploaded to {:?}", upload.link());
							//acc += upload.link().unwrap();
							let url = upload_image(&client, &buf);
							println!("uploaded to {:?}", url);
							out.write_all(url.as_bytes())?;
							out.write_all(b" ")?;
							out.flush()?;
							break;
						}
					}	
				}
			}
			buf = Vec::new();
			println!("retrying..");
		}
	}
	Ok(())
}

fn upload_image(client: &Client, buf: &[u8]) -> String {
	let mut file = "data:image/png;base64,".to_owned();
	let mut params = HashMap::new();
	params.insert("upload_preset", "unsigned_upload");
	let base64 = base64::encode(&buf);
	file.push_str(&base64);
	params.insert("file", &file);
	let json = client.post("https://api.cloudinary.com/v1_1/wynnbotcrop1/upload")
		.form(&params)
		.send().unwrap().text().unwrap();
	let data: serde_json::Value = serde_json::from_str(&json).unwrap();
	data.as_object().unwrap().get("secure_url").unwrap().as_str().unwrap().to_owned()
}

fn main() {
	let port = env::var("PORT").expect("no $PORT");
	let mut router = Router::new();

    router.add_route("crop".to_string(), |req: &mut Request| {
        Ok(Response {
			status: Some(Status::Ok),
			headers: Headers::new(),
			extensions: TypeMap::new(),
			body: Some(Box::new(Writer(req.url.clone())))
		})
    });

	Iron::new(router).http(&("0.0.0.0:".to_owned() + &port)).unwrap();
}

struct Writer(Url);

impl WriteBody for Writer {
	fn write_body(&mut self, res: &mut Write) -> Result<(), std::io::Error> {
		let url = &self.0.query().unwrap()[4..];
		if url.starts_with("https://postimg.org/gallery/") {
			postimg_gallery(res, url)?;
		} else {
			let mut buf = Vec::new();
			reqwest::get(url).unwrap().read_to_end(&mut buf).unwrap();
			let mut img = image::load(Cursor::new(&*buf), image::guess_format(&buf).unwrap()).unwrap();
			img = crop(img);
			let mut buf = Vec::new();
			img.save(&mut buf, ImageFormat::PNG).unwrap();
			//let handle = imgur::Handle::new("c819d6faf0219ac".to_owned());
			//handle.upload(&buf).unwrap().link().unwrap().to_owned()
			let image_url = upload_image(&Client::new(), &buf);
			res.write_all(&image_url.as_bytes())?;
		}
		Ok(())
	}
}

struct Router {
    // Routes here are simply matched with the url path.
    routes: HashMap<String, Box<Handler>>
}

impl Router {
    fn new() -> Self {
        Router { routes: HashMap::new() }
    }

    fn add_route<H>(&mut self, path: String, handler: H) where H: Handler {
        self.routes.insert(path, Box::new(handler));
    }
}

impl Handler for Router {
    fn handle(&self, req: &mut Request) -> IronResult<Response> {
        match self.routes.get(&req.url.path().join("/")) {
            Some(handler) => handler.handle(req),
            None => Ok(Response::with(Status::NotFound))
        }
    }
}