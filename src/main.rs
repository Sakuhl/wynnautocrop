extern crate image;
use image::ImageFormat;

extern crate wynnautocrop;
use wynnautocrop::*;

use std::env;
use std::fs::File;

fn main() {
	let imgs = env::args().skip(1).collect::<Vec<_>>();

	for img in imgs {
		let img_crop = img.clone() + ".crop.png";
		let mut img = image::open(img).unwrap();

		img = crop(img);

		img.save(&mut File::create(img_crop).unwrap(), ImageFormat::PNG).unwrap();
	}
}