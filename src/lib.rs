extern crate image;
use image::{DynamicImage, GenericImage};

extern crate palette;
use palette::{Hsv, Rgb, FromColor};

use std::collections::HashSet;

pub fn crop(mut img: DynamicImage) -> DynamicImage {
	let mut minx = 0;//img.width();
	let mut maxx = 0;
	let mut miny = 0;//img.height();
	let mut maxy = 0;
	let mut points = HashSet::new();
	for x in 0..img.width() {
		for y in 0..img.height() {
			let rgb = img.get_pixel(x, y);
			let hsv = Hsv::<f64>::from_rgb(Rgb::new_u8(rgb.data[0], rgb.data[1], rgb.data[2]));
			let hue_diff = hsv.hue.to_positive_degrees() - 265.0;
			if hue_diff > -16.0 && hue_diff < 10.0 && hsv.saturation > 0.7 && hsv.value > 0.18 {
				//img.put_pixel(x, y, RED);
				points.insert((x, y));
				/*
				if x < minx {
					minx = x;
				}
				if x > maxx {
					maxx = x;
				}
				if y < miny {
					miny = y;
				}
				if y > maxy {
					maxy = y;
				}
				*/
			}
		}
	}
	for point in &points {
		let x = point.0;
		let y = point.1;
		let mut x2 = x;
		for next_x in x.. {
			if !points.contains(&(next_x, y)) {
				x2 = next_x;
				break;
			}
		}
		let mut y2 = y;
		for next_y in y.. {
			if !points.contains(&(x, next_y)) {
				y2 = next_y;
				break;
			}
		}
		if (x2 - x) > (maxx - minx) {
			maxx = x2;
			minx = x;
		}
		if (y2 - y) > (maxy - miny) {
			maxy = y2;
			miny = y;
		}
	}
	img.crop(minx, miny, maxx-minx, maxy-miny)
}